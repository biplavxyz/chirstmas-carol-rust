fn main() {
    let gifts = ["A partridge in a pear tree.",
    "Two turtle doves and",
    "Three french hens,",
    "Four calling birds,",
    "Five gold rings,",
    "Six geese a-laying,",
    "Seven swans a-swimming,",
    "Eight maids a-milking,",
    "Nine ladies dancing,",
    "Ten lords a-leaping,",
    "Eleven pipers piping,",
    "Twelve drummers drumming,"];

    let number_of_day = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"];

    println!("*******************************************************************");
    for i in 0..12{
        println!("On the {} day of Christmas, my true love gave to me: ", number_of_day[i]);

        for gift in (0..i+1).rev() {
            println!("{}", gifts[gift]);
        }
        println!("*******************************************************************");
    }
}
